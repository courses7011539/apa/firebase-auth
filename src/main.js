import { createApp } from 'vue'
import App from './App.vue'
import router from './routers/index'
import store from './store'

import 'bootstrap/dist/css/bootstrap.css';

//Dependencias de firebase
import {initializeApp} from 'firebase/app'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCOSSP5jtCj-EFCRq91YLcPU33ea08tfT0",
  authDomain: "upa-prueba-65897.firebaseapp.com",
  projectId: "upa-prueba-65897",
  storageBucket: "upa-prueba-65897.appspot.com",
  messagingSenderId: "578714115215",
  appId: "1:578714115215:web:af4958709fedf1317d24fa",
  measurementId: "G-RXNNDCW9KN"
};
  
initializeApp(firebaseConfig);

createApp(App).use(store).use(router).mount('#app')
